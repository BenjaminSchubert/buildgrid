# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys
from urllib.parse import urlparse

import click
import grpc
import yaml

from buildgrid.server.bots.instance import BotsInterface
from buildgrid.server.controller import ExecutionController
from buildgrid.server.actioncache.instance import ActionCache
from buildgrid.server.actioncache.remote import RemoteActionCache
from buildgrid.server.actioncache.s3storage import S3ActionCache
from buildgrid.server.actioncache.writeonceaction import WriteOnceActionCache
from buildgrid.server.referencestorage.storage import ReferenceCache
from buildgrid.server.cas.instance import ByteStreamInstance, ContentAddressableStorageInstance
from buildgrid.server.cas.storage.disk import DiskStorage
from buildgrid.server.cas.storage.lru_memory_cache import LRUMemoryCache
from buildgrid.server.cas.storage.remote import RemoteStorage
from buildgrid.server.cas.storage.s3 import S3Storage
from buildgrid.server.cas.storage.with_cache import WithCacheStorage
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.server.logstream.instance import LogStreamInstance
from buildgrid.server.persistence.mem.impl import MemoryDataStore
from buildgrid.server.persistence.sql.impl import SQLDataStore
from buildgrid.settings import DEFAULT_PLATFORM_PROPERTY_KEYS, DEFAULT_MAX_EXECUTION_TIMEOUT

from ..cli import Context
from ..._enums import DataStoreType, ServiceName


class YamlFactory(yaml.YAMLObject):
    """ Base class for contructing maps or scalars from tags.
    """

    @classmethod
    def from_yaml(cls, loader, node):
        if isinstance(node, yaml.ScalarNode):
            value = loader.construct_scalar(node)
            return cls(value)

        else:
            values = loader.construct_mapping(node, deep=True)
            for key, value in dict(values).items():
                values[key.replace('-', '_')] = values.pop(key)
            return cls(**values)


class Channel(YamlFactory):
    """Creates a GRPC channel.

    The :class:`Channel` class returns a `grpc.Channel` and is generated from
    the tag ``!channel``. Creates either a secure or insecure channel.

    Usage
        .. code:: yaml

            - !channel
              port: 50051
              insecure-mode: false
              credentials:
                tls-server-key: !expand-path ~/.config/buildgrid/server.key
                tls-server-cert: !expand-path ~/.config/buildgrid/server.cert
                tls-client-certs: !expand-path ~/.config/buildgrid/client.cert

    Args:
        port (int): A port for the channel.
        insecure_mode (bool): If ``True``, generates an insecure channel, even
            if there are credentials. Defaults to ``True``.
        credentials (dict, optional): A dictionary in the form::

            tls-server-key: /path/to/server-key
            tls-server-cert: /path/to/server-cert
            tls-client-certs: /path/to/client-certs
    """

    yaml_tag = u'!channel'

    def __init__(self, port, insecure_mode, credentials=None):
        self.address = f'[::]:{port}'
        self.credentials = None

        context = Context()

        if not insecure_mode:
            server_key = credentials.get('tls-server-key')
            server_cert = credentials.get('tls-server-cert')
            client_certs = credentials.get('tls-client-certs')
            self.credentials = context.load_server_credentials(server_key, server_cert, client_certs)

            if not self.credentials:
                click.echo("ERROR: load_server_credentials could not find certificates.\n" +
                           "Please check whether the specified certificate paths exist.\n", err=True)
                sys.exit(-1)


class ExpandPath(YamlFactory):
    """Returns a string of the user's path after expansion.

    The :class:`ExpandPath` class returns a string and is generated from the
    tag ``!expand-path``.

    Usage
        .. code:: yaml

            path: !expand-path ~/bgd-data/cas

    Args:
        path (str): Can be used with strings such as: ``~/dir/to/something``
            or ``$HOME/certs``
    """

    yaml_tag = u'!expand-path'

    def __new__(cls, path):
        path = os.path.expanduser(path)
        path = os.path.expandvars(path)
        return path


class ReadFile(YamlFactory):
    """Returns a string of the contents of the specified file.

    The :class:`ReadFile` class returns a string and is generated from the
    tag ``!read-file``.

    Usage
        .. code:: yaml

            secret_key: !read-file /var/bgd/s3-secret-key

    Args:
        path (str): Can be used with strings such as: ``~/path/to/some/file``
            or ``$HOME/myfile`` or ``/path/to/file``
    """

    yaml_tag = u'!read-file'

    def __new__(cls, path):
        # Expand path
        path = os.path.expanduser(path)
        path = os.path.expandvars(path)

        if not os.path.exists(path):
            click.echo(f"ERROR: read-file `{path}` failed due to it not existing or bad permissions.",
                       err=True)
            sys.exit(-1)
        else:
            with open(path, 'r') as file:
                try:
                    file_contents = "\n".join(file.readlines()).strip()
                    return file_contents
                except IOError as e:
                    click.echo(f"ERROR: read-file failed to read file `{path}`: {e}", err=True)
                    sys.exit(-1)


class Disk(YamlFactory):
    """Generates :class:`buildgrid.server.cas.storage.disk.DiskStorage` using the tag ``!disk-storage``.

    Usage
        .. code:: yaml

            - !disk-storage
              path: /opt/bgd/cas-storage

    Args:
        path (str): Path to directory to storage.

    """

    yaml_tag = u'!disk-storage'

    def __new__(cls, path):
        """Creates a new disk

        Args:
           path (str): Some path
        """
        return DiskStorage(path)


class LRU(YamlFactory):
    """Generates :class:`buildgrid.server.cas.storage.lru_memory_cache.LRUMemoryCache` using the tag ``!lru-storage``.

    Usage
        .. code:: yaml

            - !lru-storage
              size: 2048M

    Args:
        size (int): Size e.g ``10kb``. Size parsed with
            :meth:`buildgrid._app.settings.parser._parse_size`.
    """

    yaml_tag = u'!lru-storage'

    def __new__(cls, size):
        return LRUMemoryCache(_parse_size(size))


class S3(YamlFactory):
    """Generates :class:`buildgrid.server.cas.storage.s3.S3Storage` using the tag ``!s3-storage``.

    Usage
        .. code:: yaml

            - !s3-storage
              bucket: bgd-bucket-{digest[0]}{digest[1]}
              endpoint: http://127.0.0.1:9000
              access_key: !read-file /var/bgd/s3-access-key
              secret_key: !read-file /var/bgd/s3-secret-key

    Args:
        bucket (str): Name of bucket
        endpoint (str): URL of endpoint.
        access-key (str): S3-ACCESS-KEY
        secret-key (str): S3-SECRET-KEY
    """

    yaml_tag = u'!s3-storage'

    def __new__(cls, bucket, endpoint, access_key, secret_key):
        return S3Storage(bucket, endpoint_url=endpoint, aws_access_key_id=access_key, aws_secret_access_key=secret_key)


class Redis(YamlFactory):
    """Generates :class:`buildgrid.server.cas.storage.redis.RedisStorage` using the tag ``!redis-storage``.

    Usage
        .. code:: yaml

            - !redis-storage
              host: 127.0.0.1
              port: 6379
              password: !read-file /var/bgd/redis-pass
              db: 0

    Args:
        host (str): hostname of endpoint.
        port (int): port on host.
        password (str): redis database password
        db (int) : db number
    """

    yaml_tag = u'!redis-storage'

    def __new__(cls, host, port, password=None, db=None):
        # Import here so there is no global buildgrid dependency on redis
        from buildgrid.server.cas.storage.redis import RedisStorage
        return RedisStorage(host=host, port=port, password=password, db=db)


class Remote(YamlFactory):
    """Generates :class:`buildgrid.server.cas.storage.remote.RemoteStorage`
    using the tag ``!remote-storage``.

    Usage
        .. code:: yaml

            - !remote-storage
              url: https://storage:50052/
              instance-name: main
              credentials:
                tls-server-key: !expand-path ~/.config/buildgrid/server.key
                tls-server-cert: !expand-path ~/.config/buildgrid/server.cert
                tls-client-certs: !expand-path ~/.config/buildgrid/client.cert

    Args:
        url (str): URL to remote storage. If used with ``https``, needs credentials.
        instance_name (str): Instance of the remote to connect to.
        credentials (dict, optional): A dictionary in the form::

           tls-client-key: /path/to/client-key
           tls-client-cert: /path/to/client-cert
           tls-server-cert: /path/to/server-cert
    """

    yaml_tag = u'!remote-storage'

    def __new__(cls, url, instance_name, credentials=None):
        # TODO: Context could be passed into the parser.
        # Also find way to get instance_name from parent
        # Issue 82
        context = Context()

        url = urlparse(url)
        remote = f'{url.hostname}:{url.port or 50051}'

        channel = None
        if url.scheme == 'http':
            channel = grpc.insecure_channel(remote)

        else:
            if not credentials:
                click.echo("ERROR: no TLS keys were specified and no defaults could be found.\n" +
                           "Set remote url scheme to `http` in order to deactivate" +
                           "TLS encryption.\n", err=True)
                sys.exit(-1)

            client_key = credentials['tls-client-key']
            client_cert = credentials['tls-client-cert']
            server_cert = credentials['tls-server-cert']
            credentials = context.load_client_credentials(client_key,
                                                          client_cert,
                                                          server_cert)
            if not credentials:
                click.echo("ERROR: no TLS keys were specified and no defaults could be found.\n" +
                           "Set remote url scheme to `http` in order to deactivate" +
                           "TLS encryption.\n", err=True)
                sys.exit(-1)

            channel = grpc.secure_channel(remote, credentials)

        return RemoteStorage(channel, instance_name)


class WithCache(YamlFactory):
    """Generates :class:`buildgrid.server.cas.storage.with_cache.WithCacheStorage`
    using the tag ``!with-cache-storage``.

    Usage
        .. code:: yaml

            - !with-cache-storage
              cache:
                !lru-storage
                size: 2048M
              fallback:
                !disk-storage
                path: /opt/bgd/cas-storage

    Args:
        cache (StorageABC): Storage instance to use as a cache
        fallback (StorageABC): Storage instance to use as a fallback on cache misses
    """

    yaml_tag = u'!with-cache-storage'

    def __new__(cls, cache, fallback):
        return WithCacheStorage(cache, fallback)


class SQLSchedulerConfig(YamlFactory):
    """Generates :class:`buildgrid.server.persistence.sql.impl.SQLDataStore` using
    the tag ``!sql-scheduler``.

    Usage
        .. code:: yaml

            - !sql-scheduler
              # This assumes that a storage instance is defined elsewhere
              # with a `&cas-storage` anchor
              storage: *cas-storage
              connection_string: postgresql://bgd:insecure@database/bgd
              automigrate: yes
              connection_timeout: 5

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`): Instance
            of storage to use for getting actions and storing job results. This must be
            an object constructed using a YAML tag ending in ``-storage``, for example
            ``!disk-storage``.
        connection_string (str): SQLAlchemy connection string to use for connecting
            to the database.
        automigrate (bool): Whether to attempt to automatically upgrade an existing
            DB schema to the newest version (this will also create everything from
            scratch if given an empty database).
        connection_timeout (int): Time to wait for an SQLAlchemy connection to be
            available in the pool before timing out.

    """

    yaml_tag = u'!sql-scheduler'

    def __new__(cls, storage, connection_string=None, automigrate=False,
                connection_timeout=5, **kwargs):
        try:
            return SQLDataStore(storage,
                                connection_string=connection_string,
                                automigrate=automigrate,
                                connection_timeout=connection_timeout,
                                **kwargs)
        except TypeError as type_error:
            click.echo(type_error, err=True)
            sys.exit(-1)


class SQLDataStoreConfig(YamlFactory):
    """Generates :class:`buildgrid.server.persistence.sql.impl.SQLDataStore` using
    the tag ``!sql-data-store``.

    .. warning::
        This is deprecated and only used for compatibility with old configs.

    Usage
        .. code:: yaml

            - !sql-data-store
              # This assumes that a storage instance is defined elsewhere
              # with a `&cas-storage` anchor
              storage: *cas-storage
              connection_string: postgresql://bgd:insecure@database/bgd
              automigrate: yes
              connection_timeout: 5

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`): Instance
            of storage to use for getting actions and storing job results. This must be
            an object constructed using a YAML tag ending in ``-storage``, for example
            ``!disk-storage``.
        connection_string (str): SQLAlchemy connection string to use for connecting
            to the database.
        automigrate (bool): Whether to attempt to automatically upgrade an existing
            DB schema to the newest version (this will also create everything from
            scratch if given an empty database).
        connection_timeout (int): Time to wait for an SQLAlchemy connection to be
            available in the pool before timing out.

    """

    yaml_tag = u'!sql-data-store'

    def __new__(cls, storage, connection_string=None, automigrate=False,
                connection_timeout=5, **kwargs):
        click.echo(click.style(
            "Warning: !sql-data-store YAML tag is deprecated. Use !sql-scheduler instead.",
            fg="red"
        ))
        try:
            return SQLDataStore(storage,
                                connection_string=connection_string,
                                automigrate=automigrate,
                                connection_timeout=connection_timeout,
                                **kwargs)
        except TypeError as type_error:
            click.echo(type_error, err=True)
            sys.exit(-1)


class MemorySchedulerConfig(YamlFactory):
    """Generates :class:`buildgrid.server.persistence.mem.impl.MemoryDataStore` using
    the tag ``!memory-scheduler``.

    Usage
        .. code:: yaml

            - !memory-scheduler
              # This assumes that a storage instance is defined elsewhere
              # with a `&cas-storage` anchor
              storage: *cas-storage

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`): Instance
            of storage to use for getting actions and storing job results. This must be
            an object constructed using a YAML tag ending in ``-storage``, for example
            ``!disk-storage``.

    """

    yaml_tag = u'!memory-scheduler'

    def __new__(cls, storage):
        return MemoryDataStore(storage)


class MemoryDataStoreConfig(YamlFactory):
    """Generates :class:`buildgrid.server.persistence.mem.impl.MemoryDataStore` using
    the tag ``!memory-data-store``.

    .. warning::
        This is deprecated and only used for compatibility with old configs. Use
        :class:`MemorySchedulerConfig` instead.

    Usage
        .. code:: yaml

            - !memory-data-store
              # This assumes that a storage instance is defined elsewhere
              # with a `&cas-storage` anchor
              storage: *cas-storage

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`): Instance
            of storage to use for getting actions and storing job results. This must be
            an object constructed using a YAML tag ending in ``-storage``, for example
            ``!disk-storage``.

    """

    yaml_tag = u'!memory-data-store'

    def __new__(cls, storage):
        click.echo(click.style(
            "Warning: !memory-data-store YAML tag is deprecated. Use !memory-scheduler instead.",
            fg="red"
        ))
        return MemoryDataStore(storage)


class SQL_Index(YamlFactory):
    """Generates :class:`buildgrid.server.cas.storage.index.sql.SQLIndex`
    using the tag ``!sql-index``.

    Usage
        .. code:: yaml

            - !sql-index
              # This assumes that a storage instance is defined elsewhere
              # with a `&cas-storage` anchor
              storage: *cas-storage
              connection_string: postgresql://bgd:insecure@database/bgd
              automigrate: yes
              window-size: 1000
              inclause-limit: -1
              fallback-on-get: no

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`):
            Instance of storage to use. This must be a storage object constructed using
            a YAML tag ending in ``-storage``, for example ``!disk-storage``.
        connection_string (str): SQLAlchemy connection string
        automigrate (bool): Attempt to automatically upgrade an existing DB schema to
            the newest version.
        window_size (uint): Maximum number of blobs to fetch in one SQL operation
            (larger resultsets will be automatically split into multiple queries)
        inclause_limit (int): If nonnegative, overrides the default number of variables
            permitted per "in" clause. See the buildgrid.server.cas.storage.index.sql.SQLIndex
            comments for more details.
        fallback_on_get (bool): By default, the SQL Index only fetches blobs from the
            underlying storage if they're present in the index on ``get_blob``/``bulk_read_blobs``
            requests to minimize interactions with the storage. If this is set, the index
            instead checks the underlying storage directly on ``get_blob``/``bulk_read_blobs``
            requests, then loads all blobs found into the index.
    """

    yaml_tag = u'!sql-index'

    def __new__(cls, storage, connection_string, automigrate=False,
                window_size=1000, inclause_limit=-1, fallback_on_get=False, **kwargs):
        return SQLIndex(storage=storage, connection_string=connection_string,
                        automigrate=automigrate, window_size=window_size,
                        inclause_limit=inclause_limit,
                        fallback_on_get=fallback_on_get, **kwargs)


class Execution(YamlFactory):
    """Generates :class:`buildgrid.server.execution.service.ExecutionService`
    using the tag ``!execution``.

    Usage
        .. code:: yaml

            # This assumes that the YAML anchors are defined elsewhere
            - !execution
              storage: *cas-storage
              action-cache: *remote-cache
              action-browser-url: http://localhost:8080
              scheduler: *state-database
              property-keys:
                - chrootDigest
              bot-session-keepalive-timeout: 600
              endpoints:
                - execution
                - operations
                - bots
              discard-unwatched-jobs: no
              max-execution-timeout: 7200

    Args:
        storage (:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`):
            Instance of storage to use. This must be an object constructed using
            a YAML tag ending in ``-storage``, for example ``!disk-storage``.
        action_cache (:class:`ActionCache`): Instance of action cache to use.
        action_browser_url (str): The base URL to use to generate Action Browser
            links to users
        scheduler(:class:`DataStoreInterface`): Instance of data store to use for
            the scheduler's state.
        property_keys (list): The allowed property keys for jobs
        bot_session_keepalive_timeout (int): The longest time (in seconds) we'll wait
            for a bot to send an update before it assumes it's dead. Defaults to 600s
            (10 minutes).
        permissive_bot_session: Whether to accept UpdateBotSession requests from bots that haven't explicitly
            called CreateBotSession with this particular server instance.
        endpoints (list): List of service/endpoint types to enable. Possible services are
            ``execution``, ``operations``, and ``bots``. By default all three are enabled.
        discard_unwatched_jobs (bool): Allow operation id to persist without a
            client connection.
        max_execution_timeout (int): The maximum time jobs are allowed to be in
            'OperationStage.EXECUTING'. This is a lazy check tha happens before
            job deduplication and when new peers register for an ongoing Operation.
            When this time is exceeded in executing stage, the job will be cancelled.

    """

    yaml_tag = u'!execution'

    def __new__(cls, storage, action_cache=None, action_browser_url=None, data_store=None,
                scheduler=None, property_keys=None,
                bot_session_keepalive_timeout=600, permissive_bot_session=False,
                endpoints=ServiceName.default_services(), discard_unwatched_jobs=False,
                max_execution_timeout=DEFAULT_MAX_EXECUTION_TIMEOUT):
        # If the configuration doesn't define a data store type, fallback to the
        # in-memory data store implementation from the old scheduler.
        if not (data_store or scheduler):
            scheduler = MemoryDataStore(storage)

        if data_store is not None:
            click.echo(click.style(
                "Warning: 'data-store' key in !execution config is deprecated. Use 'scheduler' instead.",
                fg="red"
            ))
            if scheduler is None:
                scheduler = data_store

        if isinstance(scheduler, MemoryDataStore) and permissive_bot_session is True:
            click.echo("ERROR: Permissive Bot Session mode is not compatible with the in-memory scheduler."
                       "Please fix the config.\n", err=True)
            sys.exit(-1)

        # Merge the property keys with the default property keys
        merged_property_keys = DEFAULT_PLATFORM_PROPERTY_KEYS.copy()
        if property_keys:
            if isinstance(property_keys, str):  # Add the string as a whole
                merged_property_keys.add(property_keys)
            else:  # Union set with an iterable object
                merged_property_keys.update(property_keys)

        click.echo(f"ExecutionService: Using {scheduler} to store state")

        return ExecutionController(scheduler, storage=storage, action_cache=action_cache,
                                   action_browser_url=action_browser_url, property_keys=merged_property_keys,
                                   bot_session_keepalive_timeout=bot_session_keepalive_timeout,
                                   permissive_bot_session=permissive_bot_session,
                                   services=endpoints, discard_unwatched_jobs=discard_unwatched_jobs,
                                   max_execution_timeout=max_execution_timeout)


class Bots(YamlFactory):
    """Generates :class:`buildgrid.server.bots.instance.BotsInterface`
    using the tag ``!bots``.

    Usage
        .. code:: yaml

            # This assumes that the YAML anchors are defined elsewhere
            - !bots
              storage: *cas-storage
              action-cache: *remote-cache
              scheduler: *state-database
              bot-session-keepalive-timeout: 600

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`):
            Instance of storage to use. This must be an object constructed using
            a YAML tag ending in ``-storage``, for example ``!disk-storage``.
        action_cache(:class:`Action`): Instance of action cache to use.
        scheduler(:class:`DataStoreInterface`): Instance of data store to use for the
            scheduler's state.
        bot_session_keepalive_timeout (int): The longest time (in seconds) we'll wait for a
            bot to send an update before it assumes it's dead. Defaults to 600s (10 minutes).
        permissive_bot_session: Whether to accept UpdateBotSession requests from bots that haven't explicitly
            called CreateBotSession with this particular server instance.
    """

    yaml_tag = u'!bots'

    def __new__(cls, storage, action_cache=None, bot_session_keepalive_timeout=600,
                data_store=None, scheduler=None, permissive_bot_session=False):
        # If the configuration doesn't define a data store type then raise
        # an error, since we can't sensibly guess what to use.
        if not (data_store or scheduler):
            click.echo("ERROR: BotsInterface configuration missing `data-store` key. "
                       "A standalone BotsInterface requires a data store to be defined.\n", err=True)
            sys.exit(-1)

        if data_store is not None:
            click.echo(click.style(
                "Warning: 'data-store' key in !bots config is deprecated. Use 'scheduler' instead.",
                fg="red"
            ))
            if scheduler is None:
                scheduler = data_store

        if isinstance(scheduler, MemoryDataStore) and permissive_bot_session is True:
            click.echo("ERROR: Permissive Bot Session mode is not compatible with the in-memory scheduler."
                       "Please fix the config.\n", err=True)
            sys.exit(-1)

        click.echo(f"BotsInterface: Using {scheduler} to store state")

        return BotsInterface(scheduler, action_cache=action_cache,
                             bot_session_keepalive_timeout=bot_session_keepalive_timeout,
                             permissive_bot_session=permissive_bot_session)


class Action(YamlFactory):
    """Generates :class:`buildgrid.server.actioncache.service.ActionCacheService`
    using the tag ``!action-cache``.

    Usage
        .. code:: yaml

            # This assumes that the YAML anchors are defined elsewhere
            - !action-cache
              storage: *cas-storage
              max-cached-refs: 1024
              cache-failed-actions: yes
              allow-updates: yes

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`):
            Instance of storage to use.
        max_cached_refs(int): Max number of cached actions.
        allow_updates(bool): Allow updates pushed to CAS. Defaults to ``True``.
        cache_failed_actions(bool): Whether to store failed (non-zero exit
            code) actions. Default to ``True``.
    """

    yaml_tag = u'!action-cache'

    def __new__(cls, storage, max_cached_refs, allow_updates=True, cache_failed_actions=True):
        return ActionCache(storage, max_cached_refs, allow_updates, cache_failed_actions)


class S3Action(YamlFactory):
    """Generates :class:`buildgrid.server.actioncache.service.S3ActionCache`
    using the tag ``!s3-action-cache``.

    Usage
        .. code:: yaml

            # This assumes that the YAML anchors are defined elsewhere
            - !s3action-cache
              storage: *cas-storage
              allow-updates: yes
              cache-failed-actions: yes
              bucket: bgd-action-cache
              endpoint: http://localhost:9000/
              access-key: !read-file /var/bgd/s3-access-key
              secret-key: !read-file /var/bgd/s3-secret-key

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`):
            Instance of storage to use. This must be an object constructed using
            a YAML tag ending in ``-storage``, for example ``!disk-storage``.
        allow_updates(bool): Allow updates pushed to CAS. Defaults to ``True``.
        cache_failed_actions(bool): Whether to store failed (non-zero exit code)
            actions. Default to ``True``.
        bucket (str): Name of bucket
        endpoint (str): URL of endpoint.
        access-key (str): S3-ACCESS-KEY
        secret-key (str): S3-SECRET-KEY

    """

    yaml_tag = u'!s3action-cache'

    def __new__(cls, storage, allow_updates=True, cache_failed_actions=True,
                bucket=None, endpoint=None, access_key=None, secret_key=None):
        return S3ActionCache(storage, allow_updates=allow_updates, cache_failed_actions=cache_failed_actions,
                             bucket=bucket, endpoint=endpoint, access_key=access_key, secret_key=secret_key)


class RemoteAction(YamlFactory):
    """Generates :class:`buildgrid.server.actioncache.remote.RemoteActionCache`
    using the tag ``!remote-action-cache``.

    Usage
        .. code:: yaml

            - !remote-action-cache
              url: https://action-cache:50053
              instance-name: main
              credentials:
                tls-server-key: !expand-path ~/.config/buildgrid/server.key
                tls-server-cert: !expand-path ~/.config/buildgrid/server.cert
                tls-client-certs: !expand-path ~/.config/buildgrid/client.cert

    Args:
        url (str): URL to remote action cache. If used with ``https``, needs credentials.
        instance_name (str): Instance of the remote to connect to.
        credentials (dict, optional): A dictionary in the form::

           tls-client-key: /path/to/client-key
           tls-client-cert: /path/to/client-cert
           tls-server-cert: /path/to/server-cert

    """

    yaml_tag = u'!remote-action-cache'

    def __new__(cls, url, instance_name, credentials=None):
        # TODO: Context could be passed into the parser.
        # Also find way to get instance_name from parent
        # Issue 82
        context = Context()

        url = urlparse(url)
        remote = f'{url.hostname}:{url.port or 50051}'

        channel = None
        if url.scheme == 'http':
            channel = grpc.insecure_channel(remote)

        else:
            if not credentials:
                click.echo("ERROR: no TLS keys were specified and no defaults could be found.\n" +
                           "Set remote url scheme to `http` in order to deactivate" +
                           "TLS encryption.\n", err=True)
                sys.exit(-1)

            client_key = credentials['tls-client-key']
            client_cert = credentials['tls-client-cert']
            server_cert = credentials['tls-server-cert']
            credentials = context.load_client_credentials(client_key,
                                                          client_cert,
                                                          server_cert)
            if not credentials:
                click.echo("ERROR: no TLS keys were specified and no defaults could be found.\n" +
                           "Set remote url scheme to `http` in order to deactivate" +
                           "TLS encryption.\n", err=True)
                sys.exit(-1)

            channel = grpc.secure_channel(remote, credentials)

        return RemoteActionCache(channel, instance_name)


class WriteOnceAction(YamlFactory):
    """Generates :class:`buildgrid.server.actioncache.remote.WriteOnceActionCache`
    using the tag ``!write-once-action-cache``.

    This allows a single update for a given key, essentially making it possible
    to create immutable ActionCache entries, rather than making the cache read-only
    as the ``allow-updates`` property of other ActionCache implementations does.

    Usage
        .. code:: yaml

            # This assumes that the YAML anchors are defined elsewhere
            - !write-once-action-cache
              action-cache: *remote-cache

    Args:
        action_cache (ActionCache): The action cache instance to make immutable.

    """

    yaml_tag = u'!write-once-action-cache'

    def __new__(cls, action_cache):
        return WriteOnceActionCache(action_cache)


class Reference(YamlFactory):
    """Generates :class:`buildgrid.server.referencestorage.service.ReferenceStorageService`
    using the tag ``!reference-cache``.

    .. note::
        This is intended for use by old versions of BuildStream as an artifact cache,
        and isn't part of the REAPI/RWAPI specifications. There's no need for this component
        if you aren't using BuildStream.

    Usage
        .. code:: yaml

            # This assumes that the YAML anchors are defined elsewhere
            - !reference-storage
              storage: *cas-storage
              max-cached-refs: 1024
              allow-updates: yes

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`):
            Instance of storage to use. This must be an object constructed using
            a YAML tag ending in ``-storage``, for example ``!disk-storage``.
        max_cached_refs(int): Max number of cached actions.
        allow_updates(bool): Allow updates pushed to CAS. Defaults to ``True``.

    """

    yaml_tag = u'!reference-cache'

    def __new__(cls, storage, max_cached_refs, allow_updates=True):
        return ReferenceCache(storage, max_cached_refs, allow_updates)


class CAS(YamlFactory):
    """Generates :class:`buildgrid.server.cas.service.ContentAddressableStorageService`
    using the tag ``!cas``.

    Usage
        .. code:: yaml

            # This assumes that the YAML anchors are defined elsewhere
            - !cas
              storage: *cas-storage

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`):
            Instance of storage to use. This must be an object constructed using
            a YAML tag ending in ``-storage``, for example ``!disk-storage``.
    """

    yaml_tag = u'!cas'

    def __new__(cls, storage, read_only=False):
        return ContentAddressableStorageInstance(storage, read_only=read_only)


class ByteStream(YamlFactory):
    """Generates :class:`buildgrid.server.cas.service.ByteStreamService`
    using the tag ``!bytestream``.

    Usage
        .. code:: yaml

            # This assumes that the YAML anchors are defined elsewhere
            - !bytestream
              storage: *cas-storage

    Args:
        storage(:class:`buildgrid.server.cas.storage.storage_abc.StorageABC`):
            Instance of storage to use. This must be an object constructed using
            a YAML tag ending in ``-storage``, for example ``!disk-storage``.
    """

    yaml_tag = u'!bytestream'

    def __new__(cls, storage, read_only=False):
        return ByteStreamInstance(storage, read_only=read_only)


class LogStream(YamlFactory):
    """Generates :class:`buildgrid.server.logstream.instance.LogStreamInstance`
    using the tag ``!logstream``.

    Usage
        .. code:: yaml

            - !logstream
              prefix: test
    """

    yaml_tag = u'!logstream'

    def __new__(cls, prefix):
        return LogStreamInstance(prefix=prefix)


def _parse_size(size):
    """Convert a string containing a size in bytes (e.g. '2GB') to a number."""
    _size_prefixes = {'k': 2 ** 10, 'm': 2 ** 20, 'g': 2 ** 30, 't': 2 ** 40}
    size = size.lower()

    if size[-1] == 'b':
        size = size[:-1]
    if size[-1] in _size_prefixes:
        return int(size[:-1]) * _size_prefixes[size[-1]]
    return int(size)


def get_parser():

    yaml.SafeLoader.add_constructor(Channel.yaml_tag, Channel.from_yaml)
    yaml.SafeLoader.add_constructor(ExpandPath.yaml_tag, ExpandPath.from_yaml)
    yaml.SafeLoader.add_constructor(ReadFile.yaml_tag, ReadFile.from_yaml)
    yaml.SafeLoader.add_constructor(Execution.yaml_tag, Execution.from_yaml)
    yaml.SafeLoader.add_constructor(Bots.yaml_tag, Bots.from_yaml)
    yaml.SafeLoader.add_constructor(Action.yaml_tag, Action.from_yaml)
    yaml.SafeLoader.add_constructor(RemoteAction.yaml_tag, RemoteAction.from_yaml)
    yaml.SafeLoader.add_constructor(S3Action.yaml_tag, S3Action.from_yaml)
    yaml.SafeLoader.add_constructor(WriteOnceAction.yaml_tag, WriteOnceAction.from_yaml)
    yaml.SafeLoader.add_constructor(Reference.yaml_tag, Reference.from_yaml)
    yaml.SafeLoader.add_constructor(Disk.yaml_tag, Disk.from_yaml)
    yaml.SafeLoader.add_constructor(LRU.yaml_tag, LRU.from_yaml)
    yaml.SafeLoader.add_constructor(S3.yaml_tag, S3.from_yaml)
    yaml.SafeLoader.add_constructor(Redis.yaml_tag, Redis.from_yaml)
    yaml.SafeLoader.add_constructor(Remote.yaml_tag, Remote.from_yaml)
    yaml.SafeLoader.add_constructor(WithCache.yaml_tag, WithCache.from_yaml)
    yaml.SafeLoader.add_constructor(SQL_Index.yaml_tag, SQL_Index.from_yaml)
    yaml.SafeLoader.add_constructor(CAS.yaml_tag, CAS.from_yaml)
    yaml.SafeLoader.add_constructor(ByteStream.yaml_tag, ByteStream.from_yaml)
    yaml.SafeLoader.add_constructor(LogStream.yaml_tag, LogStream.from_yaml)
    yaml.SafeLoader.add_constructor(SQLDataStoreConfig.yaml_tag, SQLDataStoreConfig.from_yaml)
    yaml.SafeLoader.add_constructor(MemoryDataStoreConfig.yaml_tag, MemoryDataStoreConfig.from_yaml)
    yaml.SafeLoader.add_constructor(SQLSchedulerConfig.yaml_tag, SQLSchedulerConfig.from_yaml)
    yaml.SafeLoader.add_constructor(MemorySchedulerConfig.yaml_tag, MemorySchedulerConfig.from_yaml)

    return yaml
