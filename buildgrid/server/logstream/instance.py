# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
from typing import TYPE_CHECKING, Dict
import uuid

from buildgrid._protos.google.devtools.logstream.v1alpha1.logstream_pb2 import LogStream
if TYPE_CHECKING:
    from buildgrid.server.server import Server


class LogStreamInstance:

    def __init__(self, prefix: str):
        self._logger = logging.getLogger(__name__)
        self._instance_name = None
        self._streams: Dict[str, LogStream] = {}
        self._prefix = prefix

    def register_instance_with_server(self, instance_name: str, server: 'Server') -> None:
        if self._instance_name is None:
            server.add_logstream_instance(self, instance_name)
            self.instance_name = instance_name
        else:
            raise AssertionError("Instance already registered")

    def create_logstream(self, parent: str) -> LogStream:
        response = self._streams.get(parent)
        if response is None:
            response = LogStream()
            response.name = f"{parent}/logStreams/{self._prefix}{str(uuid.uuid4())}"
            response.write_resource_name = f"{response.name}/{str(uuid.uuid4())}"
            self._streams[parent] = response
        return response
