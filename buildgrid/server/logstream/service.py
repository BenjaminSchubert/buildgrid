# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
import uuid

from grpc import ServicerContext

from buildgrid._enums import MetricRecordDomain
from buildgrid._exceptions import InvalidArgumentError
from buildgrid._protos.google.devtools.logstream.v1alpha1.logstream_pb2 import (
    CreateLogStreamRequest,
    LogStream
)
from buildgrid._protos.google.devtools.logstream.v1alpha1 import logstream_pb2_grpc
from buildgrid.server._authentication import AuthContext, authorize
from buildgrid.server.logstream.instance import LogStreamInstance
from buildgrid.server.metrics_names import (
    LOGSTREAM_CREATE_LOG_STREAM_TIME_METRIC_NAME
)
from buildgrid.server.metrics_utils import DurationMetric
from buildgrid.server.request_metadata_utils import printable_request_metadata


class LogStreamService(logstream_pb2_grpc.LogStreamServiceServicer):

    def __init__(self, server):
        self._logger = logging.getLogger(__name__)
        self._instances = {}

        logstream_pb2_grpc.add_LogStreamServiceServicer_to_server(self, server)

    def add_instance(self, name: str, instance: LogStreamInstance) -> None:
        self._instances[name] = instance

    @authorize(AuthContext)
    @DurationMetric(LOGSTREAM_CREATE_LOG_STREAM_TIME_METRIC_NAME, MetricRecordDomain.CAS)
    def CreateLogStream(self, request: CreateLogStreamRequest,
                        context: ServicerContext) -> LogStream:
        self._logger.debug(f"CreateLogStream request from [{context.peer()}]")

        instance_name, parent = request.parent.split("/", 1)
        instance = self._get_instance(instance_name)

        return instance.create_logstream(parent)

    def _get_instance(self, instance_name: str):
        try:
            return self._instances[instance_name]
        except KeyError:
            raise InvalidArgumentError(f"Invalid instance name: [{instance_name}]")
