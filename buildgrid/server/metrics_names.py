# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# CAS metrics
#

#: Number of exceptions thrown from CAS servicer functions
CAS_EXCEPTION_COUNT_METRIC_NAME = 'cas-exception'

#: Number of bytes uploaded to a CAS instance
CAS_UPLOADED_BYTES_METRIC_NAME = 'cas-uploaded-bytes'

#: Number of bytes downloaded from a CAS instance
CAS_DOWNLOADED_BYTES_METRIC_NAME = 'cas-downloaded-bytes'

#: Number of blobs requested in ``FindMissingBlobs()`` calls
CAS_FIND_MISSING_BLOBS_NUM_REQUESTED_METRIC_NAME = 'find-missing-blobs-num-requested'

#: Number of blobs reported to be missing in ``FindMissingBlobs()`` calls
CAS_FIND_MISSING_BLOBS_NUM_MISSING_METRIC_NAME = 'find-missing-blobs-num-missing'

#: Percentage of blobs reported to be missing in ``FindMissingBlobs()`` calls
CAS_FIND_MISSING_BLOBS_PERCENT_MISSING_METRIC_NAME = 'find-missing-blobs-percent-missing'

#: Time that ``FindMissingBlobs()`` operations took to complete
CAS_FIND_MISSING_BLOBS_TIME_METRIC_NAME = 'find-missing-blobs'

#: Time that ``BatchUpdateBlobs()`` operations took to complete
CAS_BATCH_UPDATE_BLOBS_TIME_METRIC_NAME = 'batch-update-blobs'

#: Time that ``BatchReadBlobs()`` operations took to complete
CAS_BATCH_READ_BLOBS_TIME_METRIC_NAME = 'batch-read-blobs'

#: Time that ``GetTree()`` operations took to complete
CAS_GET_TREE_TIME_METRIC_NAME = 'get-tree'

#: Time that ``ByteStream.Read()`` operations took to complete
CAS_BYTESTREAM_READ_TIME_METRIC_NAME = 'bytestream-read'

#: Time that ``ByteStream.Write()`` operations took to complete
CAS_BYTESTREAM_WRITE_TIME_METRIC_NAME = 'bytestream-write'

#
# ActionCache metrics
#

#: Time that ``GetActionResult()`` operations took to complete
AC_GET_ACTION_RESULT_TIME_METRIC_NAME = 'get-action-result'

#: Time that ``UpdateActionResult()`` operations took to complete
AC_UPDATE_ACTION_RESULT_TIME_METRIC_NAME = 'update-action-result'

#: Number of cache hits from the ActionCache
AC_CACHE_HITS_METRIC_NAME = 'action-cache-hits'

#: Number of cache misses from the ActionCache
AC_CACHE_MISSES_METRIC_NAME = 'action-cache-misses'


#
# S3 metrics
#

#: Time taken to check errors from a bulk_delete
S3_DELETE_ERROR_CHECK_METRIC_NAME = "s3-deletion-error-check-timer"


#
# Cleanup metrics
#

#: Total time taken to clean enough blobs to get the CAS size down to the low watermark
CLEANUP_RUNTIME_METRIC_NAME = "cleanup.runtime-timer"

#: Time taken to bulk delete a set of blobs from the index
CLEANUP_INDEX_BULK_DELETE_METRIC_NAME = "cleanup.index.bulk-delete-timer"

#: Time taken to mark a set of blobs as deleted in the index
CLEANUP_INDEX_MARK_DELETED_METRIC_NAME = "cleanup.index.mark-as-deleted-timer"

#: Number of blobs that were already marked for deletion in the index when marking as deleted
CLEANUP_INDEX_PREMARKED_BLOBS_METRIC_NAME = "cleanup.index.premarked-blobs-count"

#: Time taken to bulk delete a set of blobs from the storage backend
CLEANUP_STORAGE_BULK_DELETE_METRIC_NAME = "cleanup.storage.bulk-delete-timer"

#: Number of blobs that failed to be deleted from the storage backend in a given bulk delete request
CLEANUP_STORAGE_DELETION_FAILURES_METRIC_NAME = "cleanup.storage.deletion-failures-count"

#: Number of blobs in a bulk delete request that were already missing from the backend
CLEANUP_STORAGE_MISSING_BLOBS_METRIC_NAME = "cleanup.storage.missing-blobs-count"


#
# ExecutedActionMetadata metrics
#

#: Time spent queued before being assigned to a worker
QUEUED_TIME_METRIC_NAME = 'action-queued-time'

#: Time spent in the worker (fetching inputs + executing + uploading outputs)
WORKER_HANDLING_TIME_METRIC_NAME = 'worker-handling-time'

#: Time spent fetching inputs before execution
INPUTS_FETCHING_TIME_METRIC_NAME = 'inputs-fetching-time'

#: Time spent waiting for executions to complete
EXECUTION_TIME_METRIC_NAME = 'execution-time'

#: Time spent uploading inputs after execution
OUTPUTS_UPLOADING_TIME_METRIC_NAME = 'outputs-uploading-time'

#: Total time spent servicing an execution request (time queued +fetching inputs +
# executing + uploading outputs)
TOTAL_HANDLING_TIME_METRIC_NAME = 'total-handling-time'


#
# Execution service metrics
#

#: Number of bots connected
BOT_COUNT_METRIC_NAME = 'bots-count'

#: Number of clients connected
CLIENT_COUNT_METRIC_NAME = 'clients-count'

#: Number of leases present in the scheduler
LEASE_COUNT_METRIC_NAME = 'lease-count'

#: Number of operations present in the scheduler
OPERATION_COUNT_METRIC_NAME = 'operation-count'

#: Number of active jobs in the scheduler
JOB_COUNT_METRIC_NAME = 'job-count'

#: Average time that a job spends waiting to be executed
AVERAGE_QUEUE_TIME_METRIC_NAME = 'average-queue-time'


#
# LogStream service metrics
#

#: Time spent creating a LogStream
LOGSTREAM_CREATE_LOG_STREAM_TIME_METRIC_NAME = 'logstream.create-logstream-time'
