.. _installation:

Installation
============

.. _install-on-host:

Installation onto host machine
------------------------------

How to install BuildGrid directly onto your machine.

.. note::

   BuildGrid server currently only supports *Linux*. *macOS* and *Windows*
   platforms are **not** supported.


.. _install-host-prerequisites:

Prerequisites
~~~~~~~~~~~~~

BuildGrid only supports ``python3 >= 3.6`` but has no system requirements.
Main Python dependencies, automatically handled during installation, include:

- `boto3`_: the Amazon Web Services (AWS) SDK for Python.
- `click`_: a Python composable command line library.
- `grpcio`_: Google's `gRPC`_ Python interface.
- `janus`_: a mixed sync-async Python queue.
- `protobuf`_: Google's `protocol-buffers`_ Python interface.
- `PyYAML`_: a YAML parser and emitter for Python.

.. _boto3: https://pypi.org/project/boto3
.. _click: https://pypi.org/project/click
.. _grpcio: https://pypi.org/project/grpcio
.. _gRPC: https://grpc.io
.. _janus: https://pypi.org/project/janus
.. _protobuf: https://pypi.org/project/protobuf
.. _protocol-buffers: https://developers.google.com/protocol-buffers
.. _PyYAML: https://pypi.org/project/PyYAML
.. _install-host-source-install:

Install from sources
~~~~~~~~~~~~~~~~~~~~

BuildGrid has ``setuptools`` support. We recommend installing it in a dedicated
`virtual environment`_. In order to do so in an environment named ``env``
placed in the source tree, run:

.. code-block:: sh

   git clone https://gitlab.com/BuildGrid/buildgrid.git
   cd buildgrid
   python3 -m venv env
   env/bin/python -m pip install --upgrade setuptools pip wheel
   env/bin/python -m pip install --editable .

.. hint::

   Once created, the virtual environment can be *activated* by sourcing the
   ``env/bin/activate`` script. In an activated terminal session, simply run
   ``deactivate`` to later *deactivate* it.

Once completed, you can check that installation succeed by locally starting the
BuildGrid server with default configuration. Simply run:

.. code-block:: sh

   env/bin/bgd server start data/config/default.conf -vvv

.. note::

   The ``setup.py`` script defines extra targets, ``auth``, ``docs``,
   ``tests``, ``db`` and ``redis``. They declare required dependency for, respectively,
   authentication and authorization management, generating documentation, running
   unit-tests and different storage implementations. They can be use as helpers for
   setting up a development environment. To use them run:

   .. code-block:: sh

      env/bin/python -m pip install --editable ".[auth,docs,tests]"

.. TODO: split up other storage backends into their own requirements files

   Similarly, if using a specific storage type, install the required dependencies. For
   example, if using a redis storage backend, install the redis client by:

    .. code-block:: sh

      env/bin/python -m pip install --editable ".[redis]"

.. _virtual environment: https://docs.python.org/3/library/venv.html
