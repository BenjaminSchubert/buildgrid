#!/bin/bash

# Requires:
# -dialog
# -grep
# -ls
# -sed
# -tr


exit_on_dialog_failure () {
    dialog_rc=$?
    clear
    if [[ $dialog_rc -ne 0 ]]; then
       exit 1
    fi
}

generate_default_command() {
    options=($(tr "[:space:]+" "\n" <<< "$dialog_options"))
    # Use the input values in the appropriate places
    bgd_config_index=${options[0]}
    bgd_config=$(sed "${bgd_config_index}q;d" <<< "$config_files")

    scale_bgd="${options[1]:-1}"
    scale_pges="${options[2]:-0}"
    scale_bots="${options[3]:-1}"
    scale_clients="${options[4]:-1}"
    client_requests="${options[5]:-1}"

    cmd_split=$' \\\n'

    full_command="STRESS_TESTING_BGD_CONFIG=$bgd_config$cmd_split"
    full_command+="STRESS_TESTING_CLIENT_REQUESTS=$client_requests$cmd_split"

    full_command+=" docker-compose -f $docker_compose_image up$cmd_split"
    full_command+="  --scale buildgrid=$scale_bgd --scale bots=$scale_bots$cmd_split"
    full_command+="  --scale clients=$scale_clients --scale database=$scale_pges"
}

generate_cleanup_command() {
    options=($(tr "[:space:]+" "\n" <<< "$dialog_options"))
    # Use the input values in the appropriate places

    scale_cleanup="${options[0]:-5}"
    scale_upload="${options[1]:-1}"
    batch_size="${options[2]:-1}"
    high_watermark="${options[3]:-1}"
    low_watermark="${options[4]:-1}"
    max_file_size="${options[5]:-1}"
    test_length="${options[6]:-1}"

    cmd_split=$' \\\n'

    full_command+="CLEANUP_DELETE_BATCH_SIZE_IN_BYTES=$batch_size$cmd_split"
    full_command+="CLEANUP_START_HIGH_WATERMARK=$high_watermark$cmd_split"
    full_command+="CLEANUP_STOP_LOW_WATERMARK=$low_watermark$cmd_split"
    fill_command+="CLEANUP_MAX_UPLOAD_FILE_CONTENT_SIZE=$max_file_size"
    full_command+="CLEANUP_TEST_FOR_N_SECONDS=$test_length$cmd_split"

    full_command+=" docker-compose -f $docker_compose_image up$cmd_split"
    full_command+=" --abort-on-container-exit$cmd_split"
    full_command+="  --scale cleanup=$scale_cleanup --scale upload-random-blobs=$scale_upload"
}

config_files="$(cd mnt/configs/ && ls -1 *.yml)"
config_options=$(grep -n "" <<< "$config_files" | sed 's/:/ /')

docker_compose_image="${STRESS_TESTING_DOCKER_COMPOSE:-pges-recc-bbrht.yml}"

case "$docker_compose_image" in

docker-compose-cleanup.yml)
dialog_options=$(dialog \
            --form "Number of instances (--scale)" 15 40 0 \
            "Cleanup:"   1 1 "5"   1 15 15 0 \
            "Upload:"    2 1 "1"   2 15 15 0 \
            --form "Configure Cleanup" 15 60 0 \
            "Batch Size (Bytes):"               1 1 "500"    1 35 15 0 \
            "High Watermark (Trgger Cleanup):"  2 1 "10000"  2 35 15 0 \
            "Low Watermark (Stop Cleanup:"      3 1 "8000"   3 35 15 0 \
            "Max Upload File Content Size:"     4 1 "100"    4 35 15 0 \
            "Test Length (Seconds):"            5 1 "100"    5 35 15 0 \
            --stdout)
exit_on_dialog_failure
generate_cleanup_command
;;

*)
dialog_options=$(dialog \
            --menu "Choose BuildGrid Config to use\n(from mnt/configs):" 15 40 10 $config_options \
            --form "Number of instances (--scale)" 15 40 0 \
            "BuildGrid:" 1 1 "1"   1 15 15 0 \
            "Postgres:"  2 1 "0"   2 15 15 0 \
            "Bots:"      3 1 "2"   3 15 15 0 \
            "Clients:"   4 1 "2"   4 15 15 0 \
            --form "Number of requests per client" 10 40 0 \
            "Client Requests:" 1 1 "1" 1 15 15 0 \
            --stdout)
exit_on_dialog_failure
generate_default_command
;;
esac

extra_options=$(dialog \
            --checklist "Additional Options" 15 50 5 \
            "NO_PP" "Disable Platform Properties" "off" \
            "DETACH" "Detach docker-compose (-d)" "off" \
            "BUILD" "Build images (--build)" "off" \
            --stdout)
exit_on_dialog_failure

OIFS=$IFS; IFS=","
IFS=$OIFS

generic_options=($(tr "[:space:]+" "\n" <<< "$extra_options"))

print_only=no

for opt in "${generic_options[@]}"; do
    if [[ "$opt" == "NO_PP" ]]; then
        full_command="STRESS_TESTING_DISABLE_PLATFORM_PROPERTIES=yes$cmd_split$full_command"
    elif [[ "$opt" == "DETACH" ]]; then
        full_command+=" -d"
    elif [[ "$opt" == "BUILD" ]]; then
        full_command+=" --build"
    fi
done

echo "$full_command"

echo "$full_command" > ./start-docker-compose.sh
chmod +x ./start-docker-compose.sh

echo "Command written to ./start-docker-compose.sh"
