#!/bin/bash

test_end=$((SECONDS+$TEST_LENGTH))
casupload=/buildbox-e2e-build/recc/build/bin/casupload
while [ $SECONDS -lt $test_end ];
do
    file_name=$(mktemp XXXXXXX)
    file_size=$(shuf -i 0-$MAX_FILE_SIZE -n 1)
    head -c $file_size </dev/urandom >> $file_name
    $casupload $file_name > /dev/null 2>&1
done
