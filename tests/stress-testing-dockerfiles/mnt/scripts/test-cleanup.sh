#!/bin/bash

filled_up=
LOWER_BOUND=$(expr $LOW_WATERMARK \- $BATCH_SIZE \* 2)
UPPER_BOUND=$(expr $HIGH_WATERMARK \+ $BATCH_SIZE \* 2)

for (( i=1; i<=$TEST_LENGTH; i++ ));
do
    sleep 1

    size=$(find /minio/buildgrid-bucket -type f -print0 | \
    du -scb --files0-from=- | grep total)
    declare -i num=$(echo $size | awk '{print $1}')

    # Prevents extraneous printing until buildgrid is ready
    if [[ $num -gt 80 ]]
    then
        echo $size
    fi

    if [[ $num -gt $LOWER_BOUND ]]
    then
        filled_up=true
    fi

    if [[ $filled_up ]] && [[ $num -ge $UPPER_BOUND || $num -le $LOWER_BOUND ]]
    then
        echo "CAS size no longer within specified range: $LOWER_BOUND - $UPPER_BOUND."
        exit 1
    fi
done

if [[ $num -le $LOWER_BOUND ]]
then
    echo "CAS size remained less than lower bound: $LOWER_BOUND."
    exit 1
fi

echo Test Passed
