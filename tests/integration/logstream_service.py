# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import re
from unittest import mock

import grpc
from grpc._server import _Context
import pytest

from buildgrid._protos.google.devtools.logstream.v1alpha1.logstream_pb2 import (
    CreateLogStreamRequest,
    LogStream
)
from buildgrid.server.logstream import service
from buildgrid.server.logstream.instance import LogStreamInstance
from buildgrid.server.logstream.service import LogStreamService


server = mock.create_autospec(grpc.server)


@pytest.fixture
def context():
    cxt = mock.MagicMock(spec=_Context)
    yield cxt


@pytest.fixture(params=['', 'test-'])
def logstream_instance(request):
    yield LogStreamInstance(request.param)


@pytest.fixture()
def logstream_service(logstream_instance):
    with mock.patch.object(service, 'logstream_pb2_grpc'):
        logstream = LogStreamService(server)
        logstream.add_instance('', logstream_instance)
        yield logstream


def test_create_logstream(logstream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    response = logstream_service.CreateLogStream(request, context)
    assert isinstance(response, LogStream)

    instance = logstream_service._get_instance('')
    match = re.fullmatch(
        f'^{parent}/logStreams/{instance._prefix}[0-9a-f-]*$', response.name)
    assert match is not None


def test_create_logstream_repeated_calls(logstream_service, context):
    parent = 'test'
    request = CreateLogStreamRequest(parent=f'/{parent}')
    response_1 = logstream_service.CreateLogStream(request, context)
    response_2 = logstream_service.CreateLogStream(request, context)
    assert response_1 == response_2
